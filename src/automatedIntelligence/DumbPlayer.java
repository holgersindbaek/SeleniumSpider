package automatedIntelligence;





import game.GameBoard;
import game.Tableau;

public class DumbPlayer{
	public enum State{NORMAL, DEALING, DESPERATE, DESPERATE_DEALING};
	
	public static State state;
	public static boolean gameOver = false;
	public static boolean bullshit = false;
	public static GameBoard trueBoard;

	
	public static void makeMove() throws NoMovesPossibleException{
		trueBoard.parse();
		int max=0;
		for(Tableau i : trueBoard.columns){
			if(max<i.getStackSize()){
				max=i.getStackSize();
			}
		}
		
		if(max==13){
			throw new IllegalStateException();
		}
			
		System.out.println(state);
		if(attemptFullStack()){
			state = State.NORMAL;
			return;
		}
		
		
		switch (state){
		case DEALING:
			makeDealingMove();break;
		case NORMAL:
			makeNormalMove();break;
		case DESPERATE:
			makeDesperateMove();break;
		case DESPERATE_DEALING:
			makeDesperateDealingMove();break;
		default:
			break;
		
		}
	}
	
	private static boolean attemptFullStack() {
		for(Tableau i: trueBoard.columns){
			if(i.getMaxStackupSize()==13){
				trueBoard.fullMove(i,i.bestTarget);
				System.out.println("Moving from "+trueBoard.columns.indexOf(i)+" to "+trueBoard.columns.indexOf(i.bestTarget)+" in order to make a full stack!");
				
				
				return true;
			}
		}
		
		
		
		
		return false;
	}

	private static void makeDesperateDealingMove() {
		boolean workable = false;
		boolean done = true;
		Tableau target = null;
		
		
		int totalCardsOnBoard=0;
		for(int i=0; i<10; i++){
			totalCardsOnBoard+=trueBoard.columns.get(i).tableau.size();
		}
		
		if(totalCardsOnBoard<10){
			System.out.println("Losing game because I can't make a legal deal.  Bullshit.");
			gameOver=true;
			bullshit=true;
			return;
		}
		
		for(int i=0; i<10; i++){
			if(trueBoard.columns.get(i).tableau.size()>1){
				workable = true;
			}
			if(trueBoard.columns.get(i).tableau.size()==0){
				done=false;
				target = trueBoard.columns.get(i);
			}
		}
		
		if(done){
			trueBoard.dealNext();
			System.out.println("Dealing out!");
			state = State.NORMAL;
			return;
		}
		
		if(!workable){
			throw new IllegalStateException();
		}
		
		
		for(Tableau i : trueBoard.columns){
			if(i.tableau.size()>1){
				trueBoard.moveCards(1, i, target);
				return;
			}
		}
	}

	public static void makeNormalMove() throws NoMovesPossibleException{
		boolean madeMove = false;
		Tableau bestTableau = null;
		
		for(int j=0; j<10; j++){
			Tableau i = trueBoard.columns.get(j);
			if(!i.perfectHalfTargets.isEmpty()){
				trueBoard.halfMove(i, i.perfectHalfTargets.get(0));
				System.out.println("Organizing from "+j+" to more developed column "+i.perfectHalfTargets.get(0));
				madeMove = true;
				return;
			}
		}
		for(int j=0; j<10; j++){
			Tableau i = trueBoard.columns.get(j);
			
			if(!i.perfectTargets.isEmpty()){
				
				if(bestTableau==null 
						|| bestTableau.distanceToEmpty+getExtraScore(bestTableau)>i.distanceToEmpty+getExtraScore(i)){
					bestTableau=i;
					
				}
				madeMove=true;
			}
		}
		if(madeMove){
			
			trueBoard.fullMove(bestTableau, bestTableau.perfectTargets.get(0));
			System.out.println("Perfect Move from "+trueBoard.columns.indexOf(bestTableau)+" to "+trueBoard.columns.indexOf(bestTableau.perfectTargets.get(0)));
			
			return;
		}else{
			if(trueBoard.deals.size()>0){
				state = State.DEALING;
				makeDealingMove();
			}
			else{
				state = State.DESPERATE;
				makeDesperateMove();
			}
		}
	}
	
	
	private static void makeDesperateMove() {
		int emptyColCount = 0;
		Tableau emptyOne=null;
		for(Tableau i: trueBoard.columns){
			if(i.distanceToEmpty==0){
				emptyColCount++;
				emptyOne=i;
			}
		}
		if(emptyColCount==0){
			System.out.println("Darn, I lost");
			gameOver=true;
			return;
		}
		else{
			
			for(Tableau i: trueBoard.columns){
				if(i.distanceToEmpty>1 && !i.containsKing()){
					trueBoard.fullMove(i, emptyOne);
					System.out.println("I'm in trouble.  I hope this opens up something convenient.");
					state = State.NORMAL;
					return;
				}
			}
			for(Tableau i: trueBoard.columns){
				if(i.distanceToEmpty>1){
					trueBoard.fullMove(i, emptyOne);
					System.out.println("I'm in trouble.  I hope this opens up something convenient.");
					state = State.NORMAL;
					return;
				}
			}
		}
		
	}

	public static void makeDealingMove(){
		int emptyColCount = 0;
		Tableau emptyOne=null;
		for(Tableau i: trueBoard.columns){
			if(i.distanceToEmpty==0){
				emptyColCount++;
				emptyOne=i;
			}
		}
		
		if(emptyColCount==0){
			trueBoard.dealNext();
			System.out.println("Dealing out!");
			state = State.NORMAL;
		}
		else{
			Tableau bestTableau = null;
			for(Tableau i: trueBoard.columns){
				if(i.distanceToEmpty>1
						&& ((bestTableau==null) || (bestTableau.containsKing()) || bestTableau.distanceToEmpty>i.distanceToEmpty)){
					bestTableau = i;
				}
			}
			
			if(bestTableau != null){
				trueBoard.fullMove(bestTableau, emptyOne);
				System.out.println("Filling in from "+trueBoard.columns.indexOf(bestTableau)+" so I can deal");
				state = State.NORMAL;
				return;
			}
			
			
			
			//if you're still in this method, it means everything is well ordered, but
			//there are still empty columns.
		
			state = State.DESPERATE_DEALING;
			makeDesperateDealingMove();
		}
	}
	
	static int getExtraScore(Tableau i){
		int extraScore=0;
		if(i.containsKing()){
			extraScore+=8;
		}
		if(i.isEasilyPerfectlyCompletelyDiggable()){
			extraScore-=6;
		}
		if(i.isEasilyPerfectlyDiggable()){
			extraScore-=3;
		}
		
		if(i.distanceToUnflipped>1){
			if(i.tableau.get(i.tableau.size()-i.getStackSize()-1).value==1){
				extraScore+=2;
				System.out.println("Caught Ace lurking in "+trueBoard.columns.indexOf(i));
			}
		}
		
		extraScore+=i.faceDownCardCount();
		
		
		return extraScore;
	}

	public static boolean isGameOver() {
		return gameOver;
	}
	
	public static void resetPlayer(){
		DumbPlayer.gameOver=false;
		DumbPlayer.state=DumbPlayer.State.NORMAL;
		DumbPlayer.bullshit=false;
	}
}


class NoMovesPossibleException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7659098806914952185L;
	
}
