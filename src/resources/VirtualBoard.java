package resources;

import java.util.ArrayList;
import java.util.HashSet;

import automatedIntelligence.SmartEasyPlayer;
import automatedIntelligence.SmartEasyPlayer.State;
import game.GameBoard;
import game.Tableau;

public class VirtualBoard {
	GameBoard board = null;
	public ArrayList<MoveTree> moves= new ArrayList<>();
	public double entropy;
	public static boolean debug;
	public static boolean debugInfinity;
	
	public VirtualBoard(GameBoard copyBoard){
		board = new GameBoard(copyBoard);
	}

	public void calculateEntropy() {
		
		entropy = 0.0d;
		HashSet<Integer> values = new HashSet<>();
		
		double sumOfSquares = 0.0;
		double squareOfSums = 0.0;
		
		for(Tableau i: board.columns){
			i.process();
			
			sumOfSquares+=i.distanceToEmpty*i.distanceToEmpty;
			squareOfSums+=i.distanceToEmpty;
			
			if(i.containsKing()){
				entropy+=5.0;
				if(i.distanceToEmpty==1){
					entropy+=5.0;
				}
			}
			for(int x=1; x<=i.unflippedCards; x++){
				entropy+=4.0/x;
				
			}
			for(int x=1; x<=i.distanceToEmpty-1; x++){
				entropy+=0.5/x;
			
			}
			
			
			
			if(i.tableau.size()==0){
				entropy-=6.0;
			
			}
			else{
				
				if(i.getTopCard().value>1 && i.getTopCard().faceUp==true){
					values.add(i.getTopCard().value);
				}
			}
			
		}
		
		squareOfSums/=10;
		squareOfSums*=squareOfSums;
		
		double variance = sumOfSquares/10.0-squareOfSums;
		
		entropy+=variance*0.2;
		
		entropy-=values.size()*0.02;
		
		if(SmartEasyPlayer.state==SmartEasyPlayer.State.DEALING){
			adjustDealingEntropy();
		}
		
		
	}

	public void virtualMove(Move move) {
		board.moveCards(move, false);		
	}

	public MoveTree getBestMove() {
		MoveTree best = null;
		double bestEntropy=entropy;
		
		for(MoveTree i: moves){
			if(i.score<bestEntropy){
				bestEntropy = i.score;
				best = i;
			}
		}
		
		return best;
	}

	public boolean hasGoodMoves() {
		for(MoveTree i: moves){
			if(i.score<entropy){
				return true;
			}
		}
		
		return false;
	}
	
	public boolean checkRevealedCards(){
		for(Tableau i: board.columns){
			if(i.tableau.size()>0 && i.getTopCard().faceUp==false){
				return true;
			}
		}
		
		return false;
	}

	public static double getScore(VirtualBoard plan) {
		
		
		if(SmartEasyPlayer.plansList.contains(plan.board)){
			if(debugInfinity)System.out.println(SmartEasyPlayer.plansList.size());
			return 1_000_000;
		}
		
		if(SmartEasyPlayer.plansList.size()>1000){
			return 1000;
		}
		
		
		SmartEasyPlayer.plansList.add(plan.board);
		
		if(plan.board.cardCount()<10&&plan.board.cardCount()!=0){
			return 5_000_000;
		}
		
		double score = 1e10;
		VirtualBoard nextPlan = null;
		int cardsMoved = 0;
		
		for(int i=0; i<10; i++){
			plan.board.columns.get(i).process();
			
			for(Tableau j:plan.board.columns.get(i).perfectTargets){
				int dest = plan.board.columns.indexOf(j);
				nextPlan = new VirtualBoard(plan.board);
				cardsMoved = nextPlan.board.fullMove(i, dest, false);
				nextPlan.calculateEntropy();
				if(debug)System.out.println("Virtual move:"+cardsMoved+" "+i+" "+dest);
				if(nextPlan.checkRevealedCards()){
					score = nextPlan.entropy;
					
					if(SmartEasyPlayer.state==State.DEALING){
						score-=1000.0;
					}
					else{
						score-=6.0;
					}
				}
				else{
					score = getScore(nextPlan);
				}
				
				
				MoveTree newTree = new MoveTree(new Move(cardsMoved, i, dest), score, plan.board);
				newTree.board.moves = nextPlan.moves;
				plan.moves.add(newTree);
				
				
				
			}
			for(Integer j:plan.board.columns.get(i).perfectHalfTargets)
			{
				//breakpoint
				nextPlan = new VirtualBoard(plan.board);
				cardsMoved = nextPlan.board.halfMove(i,j);
				nextPlan.calculateEntropy();
				if(debug)System.out.println("Virtual half-move:" +cardsMoved+" "+i+" "+j);
				score = getScore(nextPlan);
				MoveTree newTree = new MoveTree(new Move(cardsMoved, i, j), score, plan.board);
				newTree.board.moves = nextPlan.moves;
				plan.moves.add(newTree);
			}
			
		}
		
		
		
		MoveTree a = plan.getBestMove();
		if(a!=null){
		return a.score;}
		else{
			return plan.entropy;
		}
		
	}

	public void planDealingMoves() {
		// TODO Auto-generated method stub
		
	}

	public void adjustDealingEntropy() {
		if(board.columns.stream().noneMatch(a->a.tableau.size()==0)){
			entropy-=1000;
		}
		
	}

	
	
	
}
