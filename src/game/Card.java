package game;

public class Card {
	public static enum Suit {
		HEART, SPADE, DIAMOND, CLUB
	};

	Card(Suit suit, int value){
		this.suit=suit; 
		this.value=value;
		this.faceUp=false;
		
	}
	
	public String webID;
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Card)){
			return false;
		}
		
		Card a = (Card) obj;
		
		return this.value==a.value&&this.suit==a.suit&&this.faceUp==a.faceUp;
		
	};
	
	@Override
	public int hashCode() {
		return value+suit.hashCode()+Boolean.hashCode(faceUp);
	}
	
	Card(Card copy){
		setSuit(copy.getSuit());
		setValue(copy.getValue());
		setFaceUp(copy.isFaceUp());
		this.webID = copy.webID;
	}
	
	public void setFaceUp(boolean faceUp){
		this.faceUp=faceUp;
	}
	
	public void flipUp(){
		faceUp=true;
	}
	
	
	public Suit getSuit() {
		return suit;
	}

	public void setSuit(Suit suit) {
		this.suit = suit;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public boolean isFaceUp() {
		return faceUp;
	}



	public Suit suit;
	public int value;
	public boolean faceUp = false;

	public String toString() {
		

		String a;

		if (value == 1)
			a = "A";
		else if (value < 10)
			a = Integer.toString(value);
		else if (value == 10)
			a = "T";
		else if (value == 11)
			a = "J";
		else if (value == 12)
			a = "Q";
		else if (value == 13)
			a = "K";
		else
			a = "X";

		switch (suit) {
		case SPADE:
			a += '♠';
			break;
		case CLUB:
			a += '♣';
			break;
		case DIAMOND:
			a += '♦';
			break;
		case HEART:
			a += '♥';
			break;
			
			
			
		default:
			throw new NullPointerException("Card suit not found");
		}
		
		if (!faceUp) {
			a="??";
		}
		
		return a;

	}
}
