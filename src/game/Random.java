package game;

public class Random {
	
	public static int getInt(int size){
		return (int)(Math.random()*size);
	}
	
	public static int getInt(int min, int max){
		return (int)(Math.random()*(max-min+1)+min);
	}
}
