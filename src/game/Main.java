package game;

import java.io.IOException;

import automatedIntelligence.DumbPlayer;
import automatedIntelligence.SmartEasyPlayer;
import resources.MoveTree;
import resources.VirtualBoard;


public class Main {
	public static void main(String[] args) throws IOException {
		
		int winCount=0;
		int gamesPlayed =500;
		for(int i=0; i<gamesPlayed; i++){
			GameBoard game = new GameBoard();
			game.dealEasyGame();
			SmartEasyPlayer.startGame(game);
			boolean win = SmartEasyPlayer.play();
			//SmartEasyPlayer.printGame();
			System.out.println(win);
			if(win){
				winCount++;
			}
			System.out.println(winCount*1.0/(i+1)*100+"% over "+(i+1)+" games.");
		}
		
		
		
		
		
		
		
		
		
		
	}
	public static void activateDebugs(){
		SmartEasyPlayer.debug=true;
				//MoveTree.debug=true;
				//VirtualBoard.debug=true;
				VirtualBoard.debugInfinity=true;
				//SmartEasyPlayer.debugMoveTree = true;
	}
	
	public static void runTests(int numGames){
		int wins = 0;;
		int games = 0;
		int bullshitCount = 0;
		
		
		for(int i=0; i<numGames; i++){
			games++;
			
			if(runGame()){
				wins++;
			}
			if(DumbPlayer.bullshit){
				bullshitCount++;
			}
		}
		
		System.out.println((double)wins/games*100+"%");
		System.out.println(bullshitCount);
	}
	
	
	static boolean runGame(){
		GameBoard table = new GameBoard();
		DumbPlayer.resetPlayer();
		DumbPlayer.trueBoard = table;
		table.dealEasyGame();
		table.printGame();
		
		
		
		while(!table.isGameOver() && !DumbPlayer.isGameOver()){
			DumbPlayer.makeMove();
			table.printGame();
			
		}
		
		return table.isGameOver();
	}
}
